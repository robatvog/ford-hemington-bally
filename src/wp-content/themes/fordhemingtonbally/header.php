<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
		<?php
			if ( is_home() && ! is_front_page() ){?>
		<title><?php bloginfo('name'); echo " | "; bloginfo('description');?></title>
		<?php } else {?>
		<title><?php the_title(); echo " | "; bloginfo('name'); ?></title>
		<?php } ?>
		<?php wp_head();
		$styleDir = get_stylesheet_directory_uri();
		?>
	</head>
	<body <?php body_class(); ?>>
		<header class="fullwidth">
			<div class="row align-middle align-justify">
				<div class="columns small-3 medium-2 large-1 logo">
					<a href="/"><img src="<?php echo $styleDir?>/images/logo.svg" alt=""></a>
				</div>
				<div class="columns small-9 medium-10 text-right toggle hide-for-large"><i class="fa fa-bars fa-fw mobile-toggle" aria-hidden="true"></i></div>
				<nav class="columns small-12 medium-12 large-7 large-offset-4"><?php wp_nav_menu('main-menu')?></nav>
			</div>
		</header>