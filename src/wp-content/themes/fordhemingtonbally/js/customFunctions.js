$(document).ready(function($){
	
	//Load Foundation!
	$(document).foundation();
	
	
	//Mobile menu toggle!
	$('.mobile-toggle').click(function(){
		$('.mobile-toggle').toggleClass('active');
		$('nav').toggleClass('open');
	});
	
	//Add classes to menu!
	$('ul#menu-main-menu').addClass('row');
	$('ul#menu-main-menu li').addClass('text-center columns small-6 medium-3');
	
	var screen = $(document).width();
	
	if( screen <= 640 ){
		$('ul#menu-main-menu li a').addClass('square');
	}
	
	// Slider Time!
	$('#hero-slider').slick({
		arrows: true,
		prevArrow: '<button type="button" class="slick-prev square"><i class="fa fa-angle-left"></i></button>',
		nextArrow: '<button type="button" class="slick-next square"><i class="fa fa-angle-right"></i></button>',
		dots: true,
		speed: 500,
		swipe: true,
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				arrows: false,
			  }
			}
		]
		});
	$('ul.slick-dots li button').empty();
	
	//Make .square containers have a height equal to their width!
	$('.square').each(function(){
		var width = $(this).width();
		var height = $(this).height();
		if(width > height){
			console.log('Height of', height);
			$(this).css('height', height);}
		if(height > width){
			console.log('Width of', width);
			$(this).css('width', width);}
		});
	
	$(window).resize(function(){
		$('.square').each(function(){
			var squareNew = $(this).width();
			$(this).removeAttr('style').css('height', squareNew);
		});
	});
	
	//Make video iframes proper aspect ratio
	$('.video-box iframe').each(function(){
		var vidWidth = $(this).width();
		var vidUnit = vidWidth/16;
		var vidHeight = vidUnit*9;
		$(this).css('height', vidHeight);
	});
	
	//History Back Button!
	$('a.history-back').click(function(){
		window.history.back();
	});
	
	//Make that form look not gross!
	$('form.wpcf7').addClass('row align-center');
	
	//Misc. Class additions for, y'know... stuff.
		
});