<?php get_header(); ?>

<div class="row " id="hero-slider">
	<?php

	// check if the repeater field has rows of data
	if( have_rows('slider') ):

		// loop through the rows of data
		while ( have_rows('slider') ) : the_row();
		$slide = get_sub_field('service_slide');
		?>
		<div class="columns small-12 hero">
			<div class="image"><img src="<?php echo get_the_post_thumbnail_url($slide->ID, 'Large-Square');?>" alt="<?php echo $slide->post_title;?>"></div>
				<div class="box">
					<h2><?php echo $slide->post_title;?></h2>
					<p><?php echo $slide->preview_text;?></p>
					<a href="<?php echo $slide->guid?>" class="button"><span>Go <i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
				</div>
		</div>

		<?php
		endwhile;

	endif;

	?>
</div>
<div class="row">
	<div class="columns small-12">
		<h1><?php bloginfo('name'); ?></h1>
		<?php the_content(); ?>
	</div>
</div>

<?php get_footer(); ?>