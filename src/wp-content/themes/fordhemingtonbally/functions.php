<?php

// Menus

function register_menus() {
	register_nav_menus(
		array(
			'main-menu' => __('Main Menu')
		)
	);
}

add_action('init', 'register_menus');


//Edit menu output

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
	
	// loop
	foreach( $items as &$item ) {
		
		// vars
		$icon = get_field('icon', $item);
		
		
		// append icon
		if( $icon ) {
			
			$item->title .= $icon;
			
		}
		
	}
	
	
	// return
	return $items;
	
}
/*Widgets
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'Calculator Area',
    'before_widget' => '<div class = "calculatorArea">',
    'after_widget' => '</div>',
  )
);*/


//Update jQuery

function update_jquery(){
    wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js');
		wp_enqueue_script('jquery');
};
add_action('wp_enqueue_scripts', 'update_jquery');

//Add Stylesheets

function stylesheets() {
	wp_enqueue_style('foundation', get_stylesheet_directory_uri() . '/css/app.css', __FILE__);
	wp_enqueue_style('fontAwesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css', __FILE__);
}

//Add Javascripts

function javascripts() {
	wp_enqueue_script('FontAwesome', 'https://use.fontawesome.com/bc77388bb6.js');
	wp_enqueue_script('foundation', get_stylesheet_directory_uri() . '/js/foundation.min.js');
	wp_enqueue_script('slick', get_stylesheet_directory_uri() . '/js/slick.min.js');
	wp_enqueue_script('customFunctions', get_stylesheet_directory_uri() . '/js/customFunctions.js');
}

add_action ('wp_enqueue_scripts', 'stylesheets');
add_action ('wp_enqueue_scripts', 'javascripts');

//Theme Supports

add_theme_support( 'post-thumbnails' );
add_post_type_support( 'page', 'excerpt' );

//Page Slug Body Class
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

//Google Maps API

/*function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyCnvAuaWoTVwK9t2mdhZZBiVy1CnsHGQCw');
}

add_action('acf/init', 'my_acf_init');*/

//Custom Image Size!

add_image_size('300x300', 300, 300, true);
add_image_size('Large-Square',600, 600, true);


//excerpt length

function new_excerpt_length($length) {
	return 50;
}

add_filter('excerpt_length', 'new_excerpt_length');

//Format Phone Numbers!

function phoneFormat($data){
	echo '('.substr($data, 0, 3).') '.substr($data, 3, 3).'-'.substr($data,6);
}

//Frikken Pagination

function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => true,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('<i class="fa fa-chevron-left"></i>'),
    'next_text'       => __('<i class="fa fa-chevron-right"></i>'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination columns small-12 text-center'>";
      //echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }
}

//Allow SVGs to be uploaded

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//Increase upload size!
@ini_set( 'upload_max_size' , '300M' );
@ini_set( 'post_max_size', '300M');
@ini_set( 'max_execution_time', '300' );