<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('WP_HOME','http://example.com');
define('WP_SITEURL','http://example.com');
 
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'database_name_here');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*R/=N.w9:xuq{*1VP}aIT>(]!D0rb)tYwF#5E<l^+-F6v9U]2df8C6lXk[GW/%@i');
define('SECURE_AUTH_KEY',  '`t*(|?|)gbB8X? ja7!mU-U3HdbaC;~Lhy,8C_u[h%:ZW`stw:UD|uFO<Z|bkV+H');
define('LOGGED_IN_KEY',    'j`}vmq}RvNGRY/IQV^$uHn4:FoF S$k1@1dr.~[R:;UG>IP!|xi4Q-qBk{D-VdPf');
define('NONCE_KEY',        'j@yaV[=2}*`&YF++}PyWR(>7+5c}:tlGXl%da5LB(_&lihz/?|9;<=)nIelXJ~Vh');
define('AUTH_SALT',        '{x!vMUTNtWHC*0!I^h0$GVDWP-?$LI0g_(HQLY|.gWWq/E`{+X<+w3m[4GF_/h@Z');
define('SECURE_AUTH_SALT', 'Z|!xfrO&lLkrC-_-z]P.eYbBEFH-d7-tAN<U[| IF3<jr-@h^q]_.0~.[1?no3SQ');
define('LOGGED_IN_SALT',   'wPOWRgeYty/|NVzI~//eh8>4;-O}0L`pdybjV=zryZb^<3#FG_h`w} @e@V8J%|=');
define('NONCE_SALT',       'ZIT-7Y>-8i:m+A!L}#5mfZ}J.|)F}t@DN4+2$+9:NB7THxXY<T0(,|*[q[zH|0 ,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
